# pipeline for web browser 4kB intros using Sointu

## Technologies and tools used
WebAssembly, WebGL, Javascript, Sointu, PnginatorModified, ShaderMinifier, Zopfli, wat2wasm, wasm-opt

Go, Java, Node.js, Mono, sed, bash, Ruby

### Sointu (tracker/synth for 4kB intro music)

"A cross-architecture and cross-platform modular software synthesizer for small intros, forked from 4klang. Targetable architectures include 386, amd64, and WebAssembly; targetable platforms include Windows, Mac, Linux (and related) + browser."

https://github.com/vsariola/sointu

## Shell scripts explained

### pack.sh 

Create a release to product directory. Pack the intro and create a zip file with the intro and the nfo-file to product directory.

### build.sh

TBA

### combine.sh

TBA

### debugshader.sh

TBA

## How to debug

TBA

## Links to used 3rd party tools etc. (in tools directory)

### Google Closure compiler
https://developers.google.com/closure/compiler

### pnginator-modified
By Gasman from an original idea by Daeken: http://daeken.com/superpacking-js-demos

### Shader Minifier
https://github.com/laurentlb/Shader_Minifier

### Sointu
https://github.com/vsariola/sointu

## Installing required tools for building and packing the intro

Example for latest stable debian distro (same should work for ubuntu).

```
Optional - install if carriage return conversion is needed for shell scripts (can occur when repo is cloned in Windows and used via WSL)
sudo apt install dos2unix

WebAssembly Binary Toolkit
sudo apt install wabt

Following is now optional (after changing how to handle and pack the Sointu webassembly), nodejs is not needed anymore!:
Toolchain for WebAssembly (installs also nodejs as dependency) -- this exact package probably is not needed, nodejs itself is needed
sudo apt install node-webassemblyjs 

compiler and toolchain infrastructure library for WebAssembly
sudo apt install binaryen

Mono runtime
sudo apt install mono-runtime

Java runtime
sudo apt install default-jre

Ruby
sudo apt install ruby

Zopfli
sudo apt install zopfli
```

## Released intros using the pipeline

### Ballad of Melon Tusk

by tmptknn & bsh, won Instanssi 2024 pikkiriikkinen (4k) demo.

- https://www.pouet.net/prod.php?which=96390
- https://instanssi.org/kompomaatti/23/compo/158/entries/1059

### Lieru

by opossumi, 5th at Instanssi 2024 pikkiriikkinen (4k) demo.

- https://instanssi.org/kompomaatti/23/compo/158/entries/1124

### CompactDiscWorld

by tmptknn & bsh, 13th at Revision 2024 pc 4k.

- https://www.pouet.net/prod.php?which=96599 
- https://files.scene.org/view/parties/2024/revision24/pc-4k-intro/compactdiscworld.zip

### The Public Knowledge of Gorilla Reef

by Bits Lab (tmptknn & bsh), 8th at Assembly Summer 2024 combined 4k.

- https://www.pouet.net/prod.php?which=97459
- https://files.scene.org/view/parties/2024/assembly_summer24/4k-intro/the-public-knowledge-of-gorilla-reef_by_bits-lab.zip
