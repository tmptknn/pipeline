if (!gl.getShaderParameter(vs, gl_COMPILE_STATUS)) {
    console.log(gl.getShaderInfoLog(vs)); // eslint-disable-line no-console
  }

if (!gl.getShaderParameter(fs, gl_COMPILE_STATUS)) {
    console.log(gl.getShaderInfoLog(fs)); // eslint-disable-line no-console
}

if (!gl.getProgramParameter(program, gl_LINK_STATUS)) {
    console.log(gl.getProgramInfoLog(program)); // eslint-disable-line no-console
}