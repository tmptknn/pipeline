#version 300 es
precision highp float;
in vec2 s; // position x=[-1,1] y=[-1,1] window
out vec4 r; // result
uniform float t; //time
uniform vec4 b;  //equalizer values except .w=1


float sdSphere( vec3 p, float s )
{
  return length(p)-s;
}

float GetDist(vec3 p){
    return sdSphere(p, 0.5*(0.5+b.x*.5))+sin(b.x*40.*p.x)*sin(b.y*40.*p.y)*sin(b.z*40.*p.z)*0.2*b.z ;
}

float RayMarch(vec3 ro, vec3 rd) {
    float dO = 0.;
    
    for (int i = 0; i < 128; i++) {
        vec3 p = ro + rd * dO;
        float dS = GetDist(p);    
        dO += dS;
        
        if (dO > 10. || dS < 0.001) break;
    }
    
    return dO;
}

vec3 GetNormal(vec3 p) {

    vec2 e = vec2(.01, 0); 
    return normalize(GetDist(p) - vec3(
        GetDist(p-e.xyy),
        GetDist(p-e.yxy),
        GetDist(p-e.yyx)
    ));
}

vec4 draw(in vec2 uv){
    vec3 rd = normalize(vec3(uv.x, uv.y, 1));
    vec3 ro = vec3(0,0,-1);
    float d = RayMarch(ro, rd);
    vec3 p = ro + rd * d;
    vec3 n = GetNormal(p);
    vec3 l = normalize(vec3(-1,2,-3));
    float dif = clamp(dot(n, l), 0.3, 1.);

    vec3 col = d<3.?dif*vec3(0.7,0.6,0.5)+b.xyz:vec3(0);
    return vec4(col, 1);
}

void main() {
    vec2 uv = s*vec2(16./9.,1.);
    r=draw(uv);
 }